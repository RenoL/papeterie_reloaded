# TP_PAPETERIE_PARTIE_02_DAL
## TP PAPETERIE – PARTIE 2 – METTRE EN OEUVRE JDBC
1. Sous SQL SERVER, créez une base de données que vous nommerez `PAPETERIE_DB`
2. Créez la table `Articles` à l’aide du fichier `script.sql` fourni
3. Complétez le projet créé dans la partie 1 avec les fichiers java fourni pour que la classe `fr.eni.papeterie.AppliTestDAL` produise le comportement voulu.
Pour cela vous devez implémentez la classe `fr.eni.papeterie.dal.jdbc.ArticleDaoJdbcImpl`