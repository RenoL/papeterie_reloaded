package fr.eni.java.tp.papeterie.ihm;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import fr.eni.java.tp.papeterie.bll.BLLException;
import fr.eni.java.tp.papeterie.bll.CatalogueManager;
import fr.eni.java.tp.papeterie.bo.Article;
import fr.eni.java.tp.papeterie.bo.Ramette;
import fr.eni.java.tp.papeterie.bo.Stylo;

import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.*;
import java.io.File;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;

/**
 * Classe gérant l'interface IHM.
 *
 * @version v5.0
 */
public class Principale extends JFrame {

    private static final String ICON_BTN_AIM     = "/img/aim.png";
    private static final String ICON_BTN_BACK    = "/img/Back24.gif";
    private static final String ICON_BTN_DELETE  = "/img/Delete24.gif";
    private static final String ICON_BTN_FORWARD = "/img/Forward24.gif";
    private static final String ICON_BTN_NEW     = "/img/New24.gif";
    private static final String ICON_BTN_SAVE    = "/img/Save24.gif";

    private        List<Article>    listeArticles = null;
    private        int              index;
    private static CatalogueManager manager;

    private static JTextField        textFieldReference;
    private static JTextField        textFieldDesignation;
    private static JTextField        textFieldMarque;
    private static JTextField        textFieldStock;
    private static JTextField        textFieldPrix;
    private static JRadioButton      radioBtnRamette;
    private static JRadioButton      radioBtnStylo;
    private static JCheckBox         chkbox80;
    private static JCheckBox         chkbox100;
    private static JComboBox<String> comboBox;

    // { x, y, w, h }
    private static final int[] BTN_BACK_DIM    = new int[]{10, 335, 56, 56};
    private static final int[] BTN_NEW_DIM     = new int[]{71, 335, 70, 56};
    private static final int[] BTN_SAVE_DIM    = new int[]{146, 335, 70, 56};
    private static final int[] BTN_DELETE_DIM  = new int[]{221, 335, 70, 56};
    private static final int[] BTN_FORWARD_DIM = new int[]{296, 335, 56, 56};

    /**
     * Launcher.
     *
     * @param args Arguments
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    Principale frame = new Principale();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public Principale() throws BLLException {
        manager = CatalogueManager.getInstance();

        // Setup frame
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100,  400 , 440);   // 362 441
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        ///// BOUTON 'NEW'
        JButton btnNew = new JButton("N");

        // Ajout d'un mouseListener.mouseClicked : sauvegarde l'article
        btnNew.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                initAjouter();
            }
        });

        // Set icone
        btnNew.setIcon(new ImageIcon(Principale.class.getResource(ICON_BTN_NEW)));
//        btnNew.setBounds(75, 335, 70, 56);
        btnNew.setBounds(BTN_NEW_DIM[0], BTN_NEW_DIM[1], BTN_NEW_DIM[2], BTN_NEW_DIM[3]);

        // Ajout du bouton au panel
        contentPane.add(btnNew);


        ///// BTN_SAVE
        // Instanciation
        JButton btnSave = new JButton("S");

        // Ajout mouseListener.mouseClicked
        btnSave.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    modifierArticle();
                } catch (BLLException bllException) {
                    bllException.printStackTrace();
                }
            }
        });

        // Set icone
        btnSave.setIcon(new ImageIcon(Principale.class.getResource(ICON_BTN_SAVE)));
//        btnSave.setBounds(145, 335, 70, 56);
        btnSave.setBounds(BTN_SAVE_DIM[0], BTN_SAVE_DIM[1], BTN_SAVE_DIM[2], BTN_SAVE_DIM[3]);

        // Ajout du bouton au panel
        contentPane.add(btnSave);


        ///// BTN_DELETE
        // Instanciation
        JButton btnDelete = new JButton("D");

        // Ajout mouseListener.mouseClicked
        btnDelete.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    deleteArticle();
                } catch (BLLException bllException) {
                    bllException.printStackTrace();
                }
            }
        });

        // Set icone
        btnDelete.setIcon(new ImageIcon(Principale.class.getResource(ICON_BTN_DELETE)));
        btnDelete.setBounds(BTN_DELETE_DIM[0], BTN_DELETE_DIM[1], BTN_DELETE_DIM[2], BTN_DELETE_DIM[3]);

        // Ajout du bouton au panel
        contentPane.add(btnDelete);


        ///// BTN_FORWARD
        // Instanciation
        JButton btnForward = new JButton("");

        // Ajout mouseListener.mouseClicked
        btnForward.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (index >= 0 && index < listeArticles.size()) {
                    index++;
                    bindDataFormulaire(listeArticles.get(index));
                }
            }
        });

        // Set icone
        btnForward.setIcon(new ImageIcon(Principale.class.getResource(ICON_BTN_FORWARD)));
        btnForward.setBounds(BTN_FORWARD_DIM[0], BTN_FORWARD_DIM[1], BTN_FORWARD_DIM[2], BTN_FORWARD_DIM[3]);

        // Ajout du bouton au panel
        contentPane.add(btnForward);


        ///// BOUTON 'BACK'
        JButton btnBack = new JButton("");

        // Ajout d'un mouseListener.mouseClicked : ramène un Article si index > 0
        btnBack.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (index > 0) {
                    index--;
                    bindDataFormulaire(listeArticles.get(index));
                }
            }
        });

        // Set icone
        btnBack.setIcon(new ImageIcon(Principale.class.getResource(ICON_BTN_BACK)));
        btnBack.setBounds(BTN_BACK_DIM[0], BTN_BACK_DIM[1], BTN_BACK_DIM[2], BTN_BACK_DIM[3]);;

        // Ajout du bouton au panel
        contentPane.add(btnBack);


        ///// LABELS
        JLabel lblReference = new JLabel("Reference");
        lblReference.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblReference.setBounds(10, 11, 68, 14);
        contentPane.add(lblReference);

        JLabel lblDesignation = new JLabel("Designation");
        lblDesignation.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblDesignation.setBounds(10, 36, 68, 14);
        contentPane.add(lblDesignation);

        JLabel lblMarque = new JLabel("Marque");
        lblMarque.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblMarque.setBounds(10, 61, 46, 14);
        contentPane.add(lblMarque);

        JLabel lblStock = new JLabel("Stock");
        lblStock.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblStock.setBounds(10, 86, 46, 14);
        contentPane.add(lblStock);

        JLabel lblPrix = new JLabel("Prix");
        lblPrix.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblPrix.setBounds(10, 111, 46, 14);
        contentPane.add(lblPrix);

        JLabel lblType = new JLabel("Type");
        lblType.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblType.setBounds(10, 155, 46, 14);
        contentPane.add(lblType);

        JLabel lblGrammage = new JLabel("Grammage");
        lblGrammage.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblGrammage.setBounds(10, 195, 68, 14);
        contentPane.add(lblGrammage);

        JLabel lblCouleur = new JLabel("Couleur");
        lblCouleur.setBounds(10, 244, 46, 14);
        lblCouleur.setFont(new Font("Tahoma", Font.BOLD, 11));
        contentPane.add(lblCouleur);


        ///// TEXT_FIELDS
        textFieldReference = new JTextField();
        textFieldReference.setColumns(10);
        textFieldReference.setBounds(90, 8, 250, 20);
        contentPane.add(textFieldReference);

        textFieldDesignation = new JTextField();
        textFieldDesignation.setColumns(10);
        textFieldDesignation.setBounds(90, 33, 250, 20);
        contentPane.add(textFieldDesignation);

        textFieldMarque = new JTextField();
        textFieldMarque.setColumns(10);
        textFieldMarque.setBounds(90, 58, 250, 20);
        contentPane.add(textFieldMarque);

        textFieldStock = new JTextField();
        textFieldStock.setColumns(10);
        textFieldStock.setBounds(90, 83, 250, 20);
        contentPane.add(textFieldStock);

        textFieldPrix = new JTextField();
        textFieldPrix.setColumns(10);
        textFieldPrix.setBounds(90, 108, 250, 20);
        contentPane.add(textFieldPrix);

        ///// RADIOS
        // radio ramette
        radioBtnRamette = new JRadioButton("Ramette");
        radioBtnRamette.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                choixRamette();
            }
        });
        radioBtnRamette.setBounds(95, 135, 109, 23);
        contentPane.add(radioBtnRamette);


        // radio stylo
        radioBtnStylo = new JRadioButton("Stylo");
        radioBtnStylo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                choixStylo();
            }
        });
        radioBtnStylo.setBounds(95, 155, 109, 23);
        contentPane.add(radioBtnStylo);


        // button group
        ButtonGroup groupe = new ButtonGroup();
        groupe.add(radioBtnRamette);
        groupe.add(radioBtnStylo);
        radioBtnRamette.setEnabled(false);
        radioBtnStylo.setEnabled(false);


        ///// CHECKBOX
        chkbox80 = new JCheckBox("80g");
        chkbox80.setBounds(95, 191, 97, 23);
        contentPane.add(chkbox80);

        chkbox100 = new JCheckBox("100g");
        chkbox100.setBounds(95, 215, 97, 23);
        contentPane.add(chkbox100);


        ///// COMBOBOX
        comboBox = new JComboBox<>();
        comboBox.addItem("Vert");
        comboBox.addItem("Jaune");
        comboBox.addItem("Rouge");
        comboBox.addItem("Violet");
        comboBox.addItem("Bleu");
        comboBox.addItem("Rose");
        comboBox.setBounds(90, 240, 250, 20);
        contentPane.add(comboBox);

        ///// BIND DATA TO ALL THAT
        bindDataDemarrage();

    }

    /**
     * Active methode removeArticle(index) du CatalogueManager et refait la GUI en accordance.
     */
    private void deleteArticle() throws BLLException {
        try {
//            Article articleACharger = listeArticles.get(index);
            manager.removeArticle(index);
            bindDataDemarrage();
        } catch (BLLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Methode complexe pour modifier un article et la GUI en même temps.
     */
    private void modifierArticle() throws BLLException {
        boolean isEnregistrer = true;
        try {
            if (isEnregistrer) {
                Article articleAEnregistrer = null;

                if (radioBtnStylo.isSelected()) {
                    // marque, ref, design, prix, stock, couleur
                    articleAEnregistrer = new Stylo(textFieldMarque.getText(), textFieldReference.getText(),
                            textFieldDesignation.getText(), Float.parseFloat(textFieldPrix.getText()),
                            Integer.parseInt(textFieldStock.getText()), (String) comboBox.getSelectedItem());
                } else {
                    // marque, ref, design, prix, stock, grammage
                    articleAEnregistrer = new Ramette(textFieldMarque.getText(), textFieldReference.getText(),
                            textFieldDesignation.getText(), Float.parseFloat(textFieldPrix.getText()),
                            Integer.parseInt(textFieldStock.getText()), chkbox80.isSelected() ? 80 : 100);
                }
                manager.addArticle(articleAEnregistrer);
            } else {
                Article articleAModifier = listeArticles.get(index);
                articleAModifier.setReference(textFieldReference.getText());
                articleAModifier.setDesignation(textFieldDesignation.getText());
                articleAModifier.setMarque(textFieldMarque.getText());
                articleAModifier.setQteStock(Integer.parseInt(textFieldStock.getText()));
                articleAModifier.setPrixUnitaire(Float.parseFloat(textFieldPrix.getText()));

                if (articleAModifier instanceof Ramette) {
                    ((Ramette) articleAModifier).setGrammage(chkbox80.isSelected() ? 80 : 100);
                }

                if (articleAModifier instanceof Stylo) {
                    ((Stylo) articleAModifier).setCouleur(comboBox.getSelectedItem().toString());
                }
                manager.addArticle(articleAModifier);

            }
        } catch (BLLException e) {
            e.printStackTrace();
        }

        bindDataDemarrage();

    }


    /**
     * Prépare un manager, un catalogue et appelle bindDataFormulaire
     */
    private void bindDataDemarrage() throws BLLException {
        try {
            listeArticles = manager.getCatalogue();
            index = 0;
            bindDataFormulaire(listeArticles.get(index));
        } catch (BLLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Series de setText, setSelected, setEnabled permettant de charger un article dans la GUI.
     * <br>Utilisé par BTN_BACK
     *
     * @param articleACharger Article à charger dans l'interface.
     */
    private void bindDataFormulaire(Article articleACharger) {
        textFieldReference.setText(articleACharger.getReference());
        textFieldDesignation.setText(articleACharger.getDesignation());
        textFieldMarque.setText(articleACharger.getMarque());
        textFieldStock.setText(String.valueOf(articleACharger.getQteStock()));
        textFieldPrix.setText(String.valueOf(articleACharger.getPrixUnitaire()));

        // Selectionne le bouton Ramette/Stylo selon instance de Article
        radioBtnRamette.setSelected(articleACharger instanceof Ramette);
        radioBtnStylo.setSelected(articleACharger instanceof Stylo);

        // Combo box se positionne sur le type d'article Ramette/Stylo
        comboBox.setEnabled(articleACharger instanceof Stylo);

        // Checkbox enabled/disabled

    }

    /**
     * Reset les textField, radio, checkbox & combobox.
     */
    private void initAjouter() {
        textFieldReference.setText("");
        textFieldDesignation.setText("");
        textFieldMarque.setText("");
        textFieldStock.setText("");
        textFieldPrix.setText("");

        radioBtnRamette.setSelected(false);
        radioBtnStylo.setSelected(false);

        radioBtnRamette.setEnabled(true);
        radioBtnStylo.setEnabled(true);

        chkbox80.setEnabled(true);
        chkbox100.setEnabled(true);

        chkbox80.setSelected(false);
        chkbox100.setSelected(false);

        comboBox.setEnabled(true);
    }

    /**
     * Set la GUI quand une ramette est choisie.
     */
    public void choixRamette() {
        chkbox80.setEnabled(true);
        chkbox80.setSelected(false);

        chkbox100.setEnabled(true);
        chkbox100.setSelected(false);

        comboBox.setEnabled(false);
    }

    /**
     * Set la GUI quand un Stylo est choisi.
     */
    public void choixStylo() {
        chkbox80.setEnabled(false);
        chkbox80.setSelected(false);

        chkbox100.setSelected(false);
        chkbox100.setEnabled(false);

        comboBox.setEnabled(true);
    }

}
