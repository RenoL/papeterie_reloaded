package fr.eni.java.tp.papeterie.utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.*;

/**
 * Classe utilitaire permettant de gérer les logger.
 *
 * @version 2.0
 */
public class MonLogger {

    public static        FileHandler    fh        = null;
    public static        ConsoleHandler ch        = null;
    private static final String         LOGDIR    = "logs/";
    private static final String         LOGPREFIX = "erreurs";

    /**
     * Permet de récuperer un logger.
     */
    public static Logger getLogger(String className) {
        Logger monLogger = Logger.getLogger(className);
        monLogger.setLevel(Level.FINEST);
        monLogger.setUseParentHandlers(false);

        if (ch == null) {
            ch = new ConsoleHandler();
            ch.setLevel(Level.FINEST);
        }

        if (fh == null) {
            try {
                fh = new FileHandler(logMaker());
            } catch (SecurityException | IOException e) {
                e.printStackTrace();
            }
            fh.setLevel(Level.ALL);
            fh.setFormatter(new SimpleFormatter());
        }

        monLogger.addHandler(ch);
        monLogger.addHandler(fh);

        return monLogger;
    }

    /**
     * Creates parent directories if necessary. Then returns file
     */
    private static String logMaker() {
        Calendar         cal       = Calendar.getInstance();
        SimpleDateFormat timestamp = new SimpleDateFormat("yyyy-MM-dd");

        File dir = new File(LOGDIR);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return LOGDIR + "/" + LOGPREFIX + "_" + timestamp.format(cal.getTime()) + ".log";
    }
}

