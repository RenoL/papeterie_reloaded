package fr.eni.java.tp.papeterie.bo;

/**
 * Classe représentant un Stylo.
 *
 * @version 1.0
 */
public class Stylo extends Article {
    private String couleur;

    public Stylo(String marque, String reference, String designation, float prixUnitaire, int qteStock,
                 String couleur) {
        super(marque, reference, designation, prixUnitaire, qteStock);
        this.couleur = couleur;
    }

    public Stylo(Integer idArticle, String marque, String reference, String designation, float prixUnitaire,
                 int qteStock, String couleur) {
        super(idArticle, marque, reference, designation, prixUnitaire, qteStock);
        this.couleur = couleur;
    }

    public Stylo() {

    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    @Override
    public String toString() {
        return "Stylo " + couleur;
    }
}
