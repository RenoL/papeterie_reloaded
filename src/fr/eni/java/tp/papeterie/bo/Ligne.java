package fr.eni.java.tp.papeterie.bo;

/**
 * Classe représentant une Ligne.
 * @version 1.0
 * @date 2020-04-27
 */
public class Ligne  {
    private Article article;
    protected int qte;

    public Ligne(Article article, int qte) {
        this.article = article;
        this.qte = qte;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public int getQte() {
        return qte;
    }

    public void setQte(int qte) {
        this.qte = qte;
    }

    public float getPrix() {
        return this.article.getPrixUnitaire() * this.qte;
    }

    @Override
    public String toString() {
        return "Ligne{qte=" + qte + ", article=" + article + ", prix=" + this.getPrix() + '}';
//        StringBuilder sb = new StringBuilder();
//        sb.append("Ligne{qte=" + qte );
//        sb.append(", prix=" + getPrix());
//        if (article != null) {
//            sb.append(", article=" + article.toString());
//        }
//        sb.append("}");
//        return sb.toString();
    }
}
