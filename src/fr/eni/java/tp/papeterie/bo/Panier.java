package fr.eni.java.tp.papeterie.bo;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant un Panier
 *
 * @version 1.0
 */
public class Panier {
    private float       montant;
    private List<Ligne> lignesPanier;

    public Panier() {
        lignesPanier = new ArrayList<>();
    }

    public float getMontant() {
        return montant;
    }

    public void setMontant(float montant) {
        this.montant = montant;
    }

    public List<Ligne> getLignesPanier() {
        return lignesPanier;
    }

    public void setLignesPanier(List<Ligne> lignesPanier) {
        this.lignesPanier = lignesPanier;
    }

    public Ligne getLigne(int index) {
        return lignesPanier.get(index);
    }

    public void addLigne(Article article, int qte) {
        lignesPanier.add(new Ligne(article, qte));
    }

    public void updateLigne(int index, int newQte) {
        lignesPanier.get(index).setQte(newQte);
    }

    public void removeLigne(int index) {
        lignesPanier.remove(index);
    }

    @Override
    public String toString() {
//        return "Panier{" + "montant=" + montant + ", lignesPanier=" + lignesPanier + '}';
        StringBuilder sb = new StringBuilder();
        sb.append("Panier{montant=" + montant);
        for (int i = 0; i < lignesPanier.size(); i++) {
            sb.append("\n ligne " + i + " " + lignesPanier.get(i));
        }
        sb.append("}");
        return sb.toString();
    }
}
