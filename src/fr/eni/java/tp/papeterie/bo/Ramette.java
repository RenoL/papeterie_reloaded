package fr.eni.java.tp.papeterie.bo;

/**
 * Classe représentant une Ramette
 *
 * @version 1.0
 */
public class Ramette extends Article {
    private int grammage;

    public Ramette(Integer idArticle, String marque, String reference, String designation, float prixUnitaire,
                   int qteStock, int grammage) {
        super(idArticle, marque, reference, designation, prixUnitaire, qteStock);
        this.grammage = grammage;
    }

    public Ramette(String marque, String reference, String designation, float prixUnitaire, int qteStock,
                   int grammage) {
        this(0, marque, reference, designation, prixUnitaire, qteStock, grammage);
    }

    public Ramette() {
        super();
    }


    public int getGrammage() {
        return grammage;
    }

    public void setGrammage(int grammage) {
        this.grammage = grammage;
    }

    @Override
    public String toString() {
        return "Ramette " + grammage + "g";
    }
}
