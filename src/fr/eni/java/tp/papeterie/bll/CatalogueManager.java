package fr.eni.java.tp.papeterie.bll;

import fr.eni.java.tp.papeterie.bo.Article;
import fr.eni.java.tp.papeterie.bo.Ramette;
import fr.eni.java.tp.papeterie.dal.ArticleDAO;
import fr.eni.java.tp.papeterie.dal.DALException;
import fr.eni.java.tp.papeterie.dal.DAOFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe faisant l'intermédiare avec les BO.
 *
 * @version 4.0
 */
public class CatalogueManager {
    private static CatalogueManager instance = null;
    private final  ArticleDAO       daoArticle;
    private        List<Article>    catalogue;

    private CatalogueManager() throws BLLException {
        daoArticle = DAOFactory.getArticleDAO();

        try {
            catalogue = daoArticle.selectAll();
        } catch (DALException ex) {
            throw new BLLException(ex.getMessage());
        }
    }

    public static CatalogueManager getInstance() throws BLLException {
        if (instance == null) {
            instance = new CatalogueManager();
        }
        return instance;
    }

    public List<Article> getCatalogue() throws BLLException {
        catalogue = new ArrayList<>();
        try {
            catalogue = daoArticle.selectAll();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new BLLException(ex.getMessage());
        }

        return catalogue;
    }

    public void updateArticle(Article article) throws BLLException {
        try {
            daoArticle.update(article);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new BLLException(ex.getMessage());
        }
    }

    public void removeArticle(int id) throws BLLException {
        try {
            daoArticle.delete(id);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new BLLException(ex.getMessage());
        }
    }

    public void validerArticle(Article article) throws BLLException {

        try {
            // check grammage > 0
            if ((article instanceof Ramette) && ((Ramette) article).getGrammage() <= 0) {
                String msg = "Le grammage d'une ramette doit être positif.";
                System.out.println(msg);
                throw new BLLException(msg);
            }

            // check prix >= 0
            if (article.getPrixUnitaire() <= 0) {
                String msg = "Le prix d'un article doit être positif.";
                System.out.println(msg);
                throw new BLLException(msg);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new BLLException(ex.getMessage());
        }
    }

    public Article getArticleById(int id) throws BLLException {
        try {
            return daoArticle.selectById(id);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new BLLException(ex.getMessage());
        }
    }

    public void addArticle(Article article) throws BLLException {
        try {
            daoArticle.insert(article);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new BLLException(ex.getMessage());
        }
    }


}
