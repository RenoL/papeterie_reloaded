package fr.eni.java.tp.papeterie.bll;

/**
 * Classe régissant les exceptions de la BLL.
 *
 * @version 4.0
 */
public class BLLException extends Exception {
    private static final String CUSTOM_MSG = "Erreur dans couche BLL - ";

    public BLLException() {
    }

    public BLLException(String message) {
        super(message);
    }

    public BLLException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public String getMessage() {
//        return super.getMessage();
        StringBuilder sb = new StringBuilder();
        sb.append(CUSTOM_MSG).append(super.getMessage());
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(CUSTOM_MSG).append(super.getMessage());
        return sb.toString();
    }
}
