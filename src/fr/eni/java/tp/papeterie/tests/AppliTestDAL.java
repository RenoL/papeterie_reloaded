package fr.eni.java.tp.papeterie.tests;

import fr.eni.java.tp.papeterie.bo.Article;
import fr.eni.java.tp.papeterie.bo.Ramette;
import fr.eni.java.tp.papeterie.bo.Stylo;
import fr.eni.java.tp.papeterie.dal.ArticleDAO;
import fr.eni.java.tp.papeterie.dal.DALException;
import fr.eni.java.tp.papeterie.dal.DAOFactory;
import fr.eni.java.tp.papeterie.dal.jdbc.ArticleDaoJdbcImpl;
import fr.eni.java.tp.papeterie.utils.MonLogger;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Test TP Papeterie v3
 */
public class AppliTestDAL {

    public static void main(String[] args) {

        Logger            logger             = MonLogger.getLogger(ArticleDaoJdbcImpl.class.getName());
        StackTraceElement stack              = new Throwable().getStackTrace()[0];
        String            nomClasseCourante  = stack.getClassName();
        String            nomMethodeCourante = stack.getMethodName();

        //Déclaration et instanciation de la DAO
//		ArticleDaoJdbcImpl articleDAO = new ArticleDaoJdbcImpl();
        ArticleDAO articleDAO = DAOFactory.getArticleDAO();

        //Instanciation du jeu d'essai
        Stylo   a1 = new Stylo("Bic", "BBOrange", "Bic bille Orange", 1.2f, 20, "bleu");
        Article a2 = new Ramette("Clairef", "CRA4S", "Ramette A4 Sup", 9f, 20, 80);
        Article a3 = new Stylo("Stypen", "PlumeS", "Stylo Plume Stypen", 5.5f, 20, "jaune");


        System.out.println("Ajout des articles... ");

        try {
            // Reset table ARTICLES
            articleDAO.resetTable();

            articleDAO.insert(a1);
            System.out.println("Article ajouté  : " + a1.toString());
            articleDAO.insert(a2);
            System.out.println("Article ajouté  : " + a2.toString());
            articleDAO.insert(a3);
            System.out.println("Article ajouté  : " + a3.toString());


            //Sélection de l'article par id
            Article a = articleDAO.selectById(a2.getIdArticle());
            System.out.println("\nSélection de l'article par id (" + a.getIdArticle() + ") : " + a.toString());

            //Sélection de tous les articles
            List<Article> articles = articleDAO.selectAll();
            System.out.println("\nSélection de tous les articles  : ");
            afficherArticles(articles);

            //Modification d'un article
            System.out.println("\nModification d'un article  : ");
            System.out.println("Article avant modification : " + a1.toString());
            a1.setCouleur("noir");
            a1.setDesignation("Bic bille noir");
            a1.setReference("BBNoir");
            articleDAO.update(a1);
            System.out.println("Article après modification  : " + a1.toString());


            //Suppression d'un article
            System.out.println("\nSuppression de l'article (id=" + a1.getIdArticle() + ") : " + a1.toString());
            articleDAO.delete(a1.getIdArticle());
            articles = articleDAO.selectAll();
            System.out.println("Liste des articles après suppression : ");
            afficherArticles(articles);
            System.out.println("---------------------------------------------------------------");


        } catch (DALException | SQLException ex) {
            logger.log(Level.SEVERE, "Erreur dans {0} / {1} : {2}", new Object[]{nomClasseCourante, nomMethodeCourante, ex.getMessage()});

            ex.printStackTrace();
        }

    }


    private static void afficherArticles(List<Article> articles) {
        StringBuilder sb = new StringBuilder();
        for (Article art : articles) {
            sb.append(art.toString());
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }
}
