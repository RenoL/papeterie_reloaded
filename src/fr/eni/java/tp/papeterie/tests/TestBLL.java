package fr.eni.java.tp.papeterie.tests;

import fr.eni.java.tp.papeterie.bll.CatalogueManager;
import fr.eni.java.tp.papeterie.bo.Article;
import fr.eni.java.tp.papeterie.bo.Ramette;

import java.util.List;

/**
 * Test pour TP Papeterie v4
 *
 * @version 4.0
 */
public class TestBLL {

    public static void main(String[] args) {
        try {

            CatalogueManager manager = CatalogueManager.getInstance();

            List<Article> liste = manager.getCatalogue();

            for (Article article : liste) {
                System.out.println(article);
            }

            System.out.println("--------------------------- AJOUT");
            // marque,reference,designation,prixUnitaire,qteStock,grammage
            manager.addArticle(new Ramette("nife", "nikera", "ramette qui court", 45, 45, 500));

            liste = manager.getCatalogue();

            for (Article article : liste) {
                System.out.println(article);
            }
            System.out.println("--------------------------- MODIF");

            Article articleAModifier = liste.get(0);
            articleAModifier.setMarque("XXX");
            articleAModifier.setPrixUnitaire(1);
            if (articleAModifier instanceof Ramette) {
                ((Ramette) articleAModifier).setGrammage(1);
            }

            manager.updateArticle(articleAModifier);

            liste = manager.getCatalogue();

            for (Article article : liste) {
                System.out.println(article);
            }

            System.out.println("--------------------------- VALIDATION");
            Article articleAValider = liste.get(0);
            manager.validerArticle(articleAValider);

            System.out.println("--------------------------- SUPPRESSION");
            manager.removeArticle(0);
            liste = manager.getCatalogue();

            for (Article article : liste) {
                System.out.println(article);
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }

    }
}
