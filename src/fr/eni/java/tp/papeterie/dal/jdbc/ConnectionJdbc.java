package fr.eni.java.tp.papeterie.dal.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Classe fournissant une connection via le pilote JDBC.
 *
 * @version 2.0
 */
public class ConnectionJdbc {
    public static Connection connection;
    private static final String DB_DETAILS = "jdbc:sqlserver://localhost:1433;encrypt=false;user=sa;password=Pa$$w0rd;database=PAPETERIE_DB;sslProtocol=TLSv1";
//    private static final String DB_DETAILS = "jdbc:jtds:sqlserver://localhost:1433;database=PAPETERIE_DB;ssl=off";


    /**
     * Constructeur privé qui définit la connection.
     */
    private ConnectionJdbc() throws SQLException {
        connection = DriverManager.getConnection(DB_DETAILS);
    }

    /**
     * Singleton qui retourne la connection.<br>
     *
     * <b>Usage:</b><pre>
     * try (Connection conn = ConnexionJdbc.getConnection()) {
     *    // stuff
     * } catch (Exception ex) {
     *    // more stuff;
     * }</pre>
     *
     * @return Un object Connection.
     */
    public static Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            new ConnectionJdbc();
        }
        return connection;
    }


}
