package fr.eni.java.tp.papeterie.dal.jdbc;

import fr.eni.java.tp.papeterie.bo.Article;
import fr.eni.java.tp.papeterie.bo.Ramette;
import fr.eni.java.tp.papeterie.bo.Stylo;
import fr.eni.java.tp.papeterie.dal.ArticleDAO;
import fr.eni.java.tp.papeterie.dal.DALException;
import fr.eni.java.tp.papeterie.dal.SQLTools;
import fr.eni.java.tp.papeterie.utils.MonLogger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implémentation de ArticleDAO.
 *
 * @version 4.0
 */
public class ArticleDaoJdbcImpl implements ArticleDAO {
    private static final String RQT_INSERT       = "INSERT INTO Articles (reference, marque, designation, " +
            "prixUnitaire, qteStock, grammage, couleur, type) VALUES (?,?,?,?, ?,?,?,?)";
    private static final String RQT_UPDATE       = "UPDATE Articles SET reference = ?,marque = ?, designation = ?, " +
            "prixUnitaire = ?, qteStock = ?, grammage = ?, couleur = ?, type = ? WHERE idArticle = ?";
    private static final String RQT_SELECT_ALL   = "SELECT * FROM Articles";
    private static final String RQT_SELECT_BY_ID = "SELECT * FROM Articles WHERE idArticle = ?";
    private static final String RQT_DELETE       = "DELETE FROM Articles WHERE idArticle = ?";

    private static final String RESET_TABLE = "./ressources/reset.sql";

    private static final String COL_ID            = "idArticle";
    private static final String COL_REFERENCE     = "reference";
    private static final String COL_MARQUE        = "marque";
    private static final String COL_DESIGNATION   = "designation";
    private static final String COL_PRIX_UNITAIRE = "prixUnitaire";
    private static final String COL_QTE           = "qteStock";
    private static final String COL_GRAMMAGE      = "grammage";
    private static final String COL_COULEUR       = "couleur";
    private static final String COL_TYPE          = "type";

    private static Logger            logger;
    private static StackTraceElement stack;
    private static String            nomMethodeCourante;
    private static String            nomClasseCourante;

    public ArticleDaoJdbcImpl() {
        logger             = MonLogger.getLogger(ArticleDaoJdbcImpl.class.getName());
        stack              = new Throwable().getStackTrace()[0];
        nomClasseCourante  = stack.getClassName();
        nomMethodeCourante = stack.getMethodName();
    }

    @Override
    public boolean insert(Article article) throws DALException {
        int nbLignesModifiees = 0;

        try (Connection connection = ConnectionJdbcByFile.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(RQT_INSERT, Statement.RETURN_GENERATED_KEYS);

            // Préparation de la requête
            // ([idArticle,] reference, marque, designation, prixUnitaire, qteStock, grammage, couleur, type)
            ps = psBuilder(article, ps);

            // Execution de la requête
            nbLignesModifiees = ps.executeUpdate();

            // Récupération & attribution de l'id (PK) de l'article inséré
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                article.setIdArticle(rs.getInt(1));
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Erreur dans {0} / {1} : {2}", new Object[]{nomClasseCourante,
                                                                                 nomMethodeCourante, ex.getMessage()});
            throw new DALException(ex.getMessage());
        }

        return nbLignesModifiees > 0;
    }

    @Override
    public Article selectById(int idArticle) throws DALException {
        Article article = null;

        try (Connection connection = ConnectionJdbcByFile.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(RQT_SELECT_BY_ID);

            ps.setInt(1, idArticle);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                article = articleBuilder(rs);
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Erreur dans {0} / {1} : {2}", new Object[]{nomClasseCourante,
                                                                                 nomMethodeCourante, ex.getMessage()});
            throw new DALException(ex.getMessage());
        }

        return article;
    }

    @Override
    public List<Article> selectAll() throws DALException {
        List<Article> articles = new ArrayList<>();

        try (Connection connection = ConnectionJdbcByFile.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(RQT_SELECT_ALL);

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                articles.add(articleBuilder(rs));
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Erreur dans {0} / {1} : {2}", new Object[]{nomClasseCourante,
                                                                                 nomMethodeCourante, ex.getMessage()});
            throw new DALException(ex.getMessage());
        }

        return articles;
    }

    @Override
    public boolean update(Article article) throws DALException {
        int nbLignesModifiees = 0;

        try (Connection connection = ConnectionJdbcByFile.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(RQT_UPDATE, Statement.RETURN_GENERATED_KEYS);

            // Préparation de la requête
            // "UPDATE Articles SET reference = ?, marque = ?, designation = ?,
            // prixUnitaire = ?, qteStock = ?, grammage = ?,
            // couleur = ?, type = ? WHERE idArticle = ?";
            ps = psBuilder(article, ps);

            // Ajout id
            ps.setInt(9, article.getIdArticle());

            // Execution de la requête
            nbLignesModifiees = ps.executeUpdate();

        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Erreur dans {0} / {1} : {2}", new Object[]{nomClasseCourante,
                                                                                 nomMethodeCourante, ex.getMessage()});
            throw new DALException(ex.getMessage());
        }

        return nbLignesModifiees > 0;
    }

    @Override
    public boolean delete(int idArticle) throws DALException {
        int nbLignesModifiees = 0;

        try (Connection connection = ConnectionJdbcByFile.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(RQT_DELETE);

            // Préparation de la requête
            ps.setInt(1, idArticle);

            // Execution de la requête
            nbLignesModifiees = ps.executeUpdate();

        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Erreur dans {0} / {1} : {2}", new Object[]{nomClasseCourante,
                                                                                 nomMethodeCourante, ex.getMessage()});
            throw new DALException(ex.getMessage());
        }

        return nbLignesModifiees > 0;
    }

    @Override
    public void resetTable() throws DALException {
        try {
            SQLTools.executeQueryScript("src/fr/eni/java/tp/papeterie/ressources/reset.sql");
//           SQLTools.executeQueryScript(RESET_TABLE);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.log(Level.SEVERE, "Erreur dans {0} / {1} : {2}", new Object[]{nomClasseCourante,
                                                                                 nomMethodeCourante, ex.getMessage()});
            throw new DALException(ex.getMessage());
        }
    }

    /**
     * Construit un Article à partir d'un ResultSet.
     *
     * @param rs ResultSet
     * @return Article
     */
    @Override
    public Article articleBuilder(ResultSet rs) throws SQLException, DALException {
        // reference, marque, designation, prixUnitaire, qteStock, grammage, couleur, type
        Article article = null;
        String  type    = rs.getString("type").toLowerCase().trim();
        try {
//            String type = rs.getString("type");

            if (type.equals("ramette")) {
                article = new Ramette(rs.getInt("idArticle"), rs.getString("marque"), rs.getString("reference"),
                        rs.getString("designation"), rs.getFloat("prixUnitaire"), rs.getInt("qteStock"), rs.getInt(
                        "grammage"));
            }

            if (type.equals("stylo")) {
                article = new Stylo(rs.getInt("idArticle"), rs.getString("marque"), rs.getString("reference"),
                        rs.getString("designation"), rs.getFloat("prixUnitaire"), rs.getInt("qteStock"),
                        rs.getString("couleur"));
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Erreur dans {0} / {1} : {2}", new Object[]{nomClasseCourante,
                                                                                 nomMethodeCourante, ex.getMessage()});
            throw new DALException(ex.getMessage());
        }

        return article;
    }

    /**
     * Construit un PreparedStatement à partir d'un Article.
     *
     * @param article Article
     * @return PreparedStatement
     */
    @Override
    public PreparedStatement psBuilder(Article article, PreparedStatement ps) throws DALException {
        try {
            // Préparation de la requête
            ps.setString(1, article.getReference());
            ps.setString(2, article.getMarque());
            ps.setString(3, article.getDesignation());
            ps.setDouble(4, article.getPrixUnitaire());
            ps.setInt(5, article.getQteStock());

            if (article instanceof Ramette) {
                ps.setInt(6, ((Ramette) article).getGrammage());
                ps.setNull(7, Types.VARCHAR);
                ps.setString(8, "Ramette");
            }

            if (article instanceof Stylo) {
                ps.setNull(6, Types.INTEGER);
                ps.setString(7, ((Stylo) article).getCouleur());
                ps.setString(8, "Stylo");
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Erreur dans {0} / {1} : {2}", new Object[]{nomClasseCourante,
                                                                                 nomMethodeCourante, ex.getMessage()});
            throw new DALException(ex.getMessage());
        }

        return ps;
    }


}
