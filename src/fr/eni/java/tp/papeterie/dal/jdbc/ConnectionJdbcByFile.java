package fr.eni.java.tp.papeterie.dal.jdbc;

import fr.eni.java.tp.papeterie.dal.Settings;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionJdbcByFile {

    private static String DB_URL;
    private static String DB_USER;
    private static String DB_PSWD;
    private static String DB_OPTIONS;
    private static String DB_DRIVER;

    public static Connection connection;

    // Bloc d'initialisation statique
    static {
        try {
            Class.forName(Settings.getProperties("DB_DRIVER"));
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        DB_DRIVER = Settings.getProperties("DB_DRIVER");

        DB_URL     = Settings.getProperties("DB_URL");
        DB_USER    = Settings.getProperties("DB_USER");
        DB_PSWD    = Settings.getProperties("DB_PSWD");
        DB_OPTIONS = Settings.getProperties("DB_OPTIONS");

        System.err.println("Getting parameters from file " + DB_URL);

    }

    // Constructeur privé
    private ConnectionJdbcByFile() throws SQLException {
        connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PSWD);
    }

    public static Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            new ConnectionJdbcByFile();
        }
        return connection;
    }
}
