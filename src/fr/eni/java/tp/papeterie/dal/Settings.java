package fr.eni.java.tp.papeterie.dal;

import java.util.Properties;

/**
 * Classe permettant de lire les paramètres d'un fichier xml ou properties.<br>
 * Voir jdbc.JdbcTools pour usage.
 *
 * @version 4.0
 */
public class Settings {
    private static Properties properties;

    private static final String SETTINGS_PROPERTIES = "settings.properties";
//    private static final String SETTINGS_XML = "settings.xml";

    static {
        try {
            properties = new Properties();

            // Read *.properties
            properties.load(Settings.class.getResourceAsStream(SETTINGS_PROPERTIES));

            // Read *.xml
//            properties.loadFromXML(Settings.class.getResourceAsStream(SETTINGS_XML));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * Retourne la valeur du paramètre correspondant à une clé donnée.
     *
     * @param key Clé (ex. key=param)
     * @return La valeur du paramètre correspondant (`null` par défaut)
     */
    public static String getProperties(String key) {
        return properties.getProperty(key, null);
    }

}
