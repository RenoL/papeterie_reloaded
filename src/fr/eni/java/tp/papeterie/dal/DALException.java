package fr.eni.java.tp.papeterie.dal;

/**
 * Classe régissant les exceptions de la DAL.
 *
 * @version 2.0
 */
public class DALException extends Exception {
    private static final String CUSTOM_MSG = "Erreur dans couche DAL - ";

    public DALException() {
        super();
    }

    public DALException(String message) {
        super(message);
    }

    public DALException(String message, Exception cause) {
        super(message, cause);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(CUSTOM_MSG).append(super.getMessage());
        return sb.toString();
    }
}
