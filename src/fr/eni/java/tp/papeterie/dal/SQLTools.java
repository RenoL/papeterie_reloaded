package fr.eni.java.tp.papeterie.dal;

import fr.eni.java.tp.papeterie.dal.jdbc.ConnectionJdbcByFile;
import fr.eni.java.tp.papeterie.utils.MonLogger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe contenant des méthodes utilitaires pour la gestion de base.
 *
 * @version 4.0
 */
public class SQLTools {
    private static Logger            logger;
    private static String            nomMethodeCourante;
    private static String            nomClasseCourante;

    /**
     * Constructeur.
     */
    public SQLTools() {
        logger = MonLogger.getLogger(SQLTools.class.getName());
        StackTraceElement stack = new Throwable().getStackTrace()[0];
        nomClasseCourante = stack.getClassName();
        nomMethodeCourante = stack.getMethodName();
    }

    /**
     * Méthode permettant d'executer un script SQL passé en paramètre.
     *
     * @param scriptFilePath chemin d'accès au script SQL
     * @throws DALException
     */
    public static void executeQueryScript(String scriptFilePath) throws DALException,
            FileNotFoundException {
        BufferedReader reader = null;
//        BufferedReader reader    = new BufferedReader(new FileReader(scriptFilePath));
        Statement statement = null;


        try (Connection conn = ConnectionJdbcByFile.getConnection()) {
            // Create statement object
            statement = conn.createStatement();

            // Init file reader
            reader = new BufferedReader(new FileReader(scriptFilePath));
            String line = null;

            // Read script line by line
            while ((line = reader.readLine()) != null) {
                // Execute query
                statement.execute(line);
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Erreur dans {0} / {1} : {2}", new Object[]{nomClasseCourante,
                    nomMethodeCourante, ex.getMessage()});
            ex.printStackTrace();
            throw new DALException(ex.getMessage());
        }
    }

}
