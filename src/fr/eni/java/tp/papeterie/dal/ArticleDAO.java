package fr.eni.java.tp.papeterie.dal;

import fr.eni.java.tp.papeterie.bo.Article;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface contenant les méthodes permettant d'intéragir avec la table ARTICLES.
 *
 * @version 2.0
 */
public interface ArticleDAO {

    /**
     * Insert un article dans la table ARTICLES.
     *
     * @param article Article à insérer
     * @return vrai si modifie plus de 0 lignes
     */
    public boolean insert(Article article) throws SQLException, DALException;

    /**
     * Select un article par son id.
     *
     * @param idArticle Id de l'article à trouver.
     * @return L'article trouvé
     */
    public Article selectById(int idArticle) throws DALException;

    /**
     * Select tout les articles de la table.
     *
     * @return Une liste d'articles.
     */
    public List<Article> selectAll() throws DALException;

    /**
     * Modifie un article.
     *
     * @param a1 L'article à modifier.
     * @return vrai si modifie plus de 0 lignes
     */
    public boolean update(Article a1) throws DALException;

    /**
     * Supprime un article.
     *
     * @param idArticle Id de l'article à supprimer.
     * @return vrai si modifie plus de 0 lignes
     */
    public boolean delete(int idArticle) throws DALException;

    /**
     * Reset la table : delete & reset PK.
     *
     * @return vrai si modifie plus de 0 lignes
     */
    public void resetTable() throws DALException, SQLException;

    /**
     * Construit un Article à partir d'un ResultSet.
     *
     * @param rs ResultSet
     * @return Article
     */
    public Article articleBuilder(ResultSet rs) throws SQLException, DALException;

    /**
     * Construit un PreparedStatement à partir d'un Article.
     *
     * @param article Article
     * @return PreparedStatement
     */
    public PreparedStatement psBuilder(Article article, PreparedStatement ps) throws SQLException, DALException;
}
