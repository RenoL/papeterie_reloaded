package fr.eni.java.tp.papeterie.dal;

import fr.eni.java.tp.papeterie.dal.jdbc.ArticleDaoJdbcImpl;

/**
 * Classe distribuant les connections à la base selon le pilote choisi.
 *
 * @version 2.0
 */
public class DAOFactory {

    public static ArticleDAO getArticleDAO() {
        return new ArticleDaoJdbcImpl();
    }

    public static ArticleDAO getArticleDAO(String pilote) throws DALException {
        switch (pilote) {
            case "mysql":
                throw new DALException("La gestion de base mysql n'est pas encore implémentée.");
            default:
                return new ArticleDaoJdbcImpl();
        }
    }

}
